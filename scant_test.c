#include <assert.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

#define SCANT_IMPL
#include "scant.h"

int scan_name(const char *source, char *name, size_t name_len) {
	// Things we know:
	// The string starts with `The`.
	// The name starts at the second uppercase alphabet in the string.
	// The name is two words long.
	// The name only has alphabets.

	static char ignore[1024];

	size_t count;
	uint32_t s = 0;
	size_t idx = 0;
	int well_ended = 0;

	// Scan the `T` of `The`.
	count = scant_char(source + idx, &s, 'T');
	if (count == 0) {
		return 0;
	}

	idx += count;

	// Scan till the first name.
	count = scant_till_charset(source + idx, ignore, sizeof(ignore) / sizeof(ignore[0]),
							   &well_ended, CHARSET_UPPERCASE_ASCII);
	if (!well_ended) {
		return 0;
	}

	idx += count;

	// Scan first name into the `name` buffer.
	count = scant_while_charset(source + idx, name, name_len, &well_ended, CHARSET_ALPHABET_ASCII);
	if (!well_ended) {
		return 0;
	}

	idx += count;

	// Add ' ' as a seprator between the first name and the secondn name.
	size_t name_idx = count;
	name[name_idx] = ' ';
	name_idx += 1;

	// Scan till the second name.
	count = scant_till_charset(source + idx, ignore, sizeof(ignore) / sizeof(ignore[0]),
							   &well_ended, CHARSET_UPPERCASE_ASCII);
	if (!well_ended) {
		return 0;
	}
	idx += count;

	// Scan the second name into the `name` buffer.
	count = scant_while_charset(source + idx, name + name_idx, name_len - name_idx, &well_ended,
								CHARSET_ALPHABET_ASCII);
	if (!well_ended) {
		return 0;
	}

	return 1;
}

void scanc_str_test() {
	{
		const char source[] = "नमस्ते";
		char destination[sizeof(source) / sizeof(source[0])];
		const size_t len = sizeof(source) / sizeof(source[0]);
		const uint32_t delimiter[] = {2381};
		int well_ended;

		const size_t size = scant_till_delimiter(source, destination, len, &well_ended, delimiter,
												 sizeof(delimiter) / sizeof(delimiter[0]));
		const size_t valid_len = 9;
		assert(well_ended && size == valid_len);
		assert(destination[valid_len] == '\0');
		assert(strcmp(destination, "नमस") == 0);
	}
	{
		const char source[] = "Ab4cu5$Hmm?";
		char destination[sizeof(source) / sizeof(source[0])];
		const size_t len = sizeof(source) / sizeof(source[0]);
		int well_ended;

		size_t size = scant_while_charset(source, destination, len, &well_ended,
										  CHARSET_ALPHABET_DIGIT_ASCII);
		const size_t valid_len = 6;
		assert(well_ended && size == valid_len);
		assert(destination[valid_len] == '\0');
		assert(strncmp(destination, source, 6) == 0);
	}
	{
		const char source[] = "Ab4cu5$Hmm?";
		char destination[sizeof(source) / sizeof(source[0])];
		const size_t len = sizeof(source) / sizeof(source[0]);
		int well_ended;

		size_t size = scant_till_char(source, destination, len, &well_ended, '$');
		const size_t valid_len = 6;
		assert(well_ended && size == valid_len);
		assert(destination[6] == '\0');
		assert(strncmp(destination, source, 6) == 0);
	}

	{
		const char source[] = "The great detective of 13th street, Sherlock    Holmes, has blessed "
							  "us with his presence.";
		char name[sizeof(source) / sizeof(source[0])];
		const size_t name_len = sizeof(source) / sizeof(source[0]);
		int success = scan_name(source, name, name_len);
		assert(success);
		assert(strcmp(name, "Sherlock Holmes") == 0);
	}
}

int main() {
	{
		const char *str = "123";
		int32_t x;
		size_t size = scant_i32_radix(str, &x, 10);
		assert(size == 3 && x == 123);
	}
	{
		const char *str = "0";
		int32_t x;
		size_t size = scant_i32_radix(str, &x, 10);
		assert(size == 1 && x == 0);
	}
	{
		const char *str = "xyz";
		int32_t x;
		size_t size = scant_i32_radix(str, &x, 10);
		assert(size == 0);
	}
	{
		const char *str = "+0";
		int32_t x;
		size_t size = scant_i32(str, &x);
		assert(size == 2 && x == 0);
	}
	{
		const char *str = "-0";
		int32_t x;
		size_t size = scant_i32(str, &x);
		assert(size == 2 && x == 0);
	}
	{
		const char *str = "-DeAd";
		int32_t x;
		size_t size = scant_i32_radix(str, &x, 16);
		assert(size == 5 && x == -0xdead);
	}
	{
		const char *str = "2147483647";
		int32_t x;
		size_t size = scant_i32_radix(str, &x, 10);
		assert(size == 10 && x == 2147483647 && x == 0x7FFFFFFF);
	}
	{
		const char *str = "FFFFFFFF";
		int32_t x;
		size_t size = scant_i32_radix(str, &x, 16);
		assert(size == 0);
	}
	{
		const char *str = "FFFFFFFF";
		uint32_t x;
		size_t size = scant_u32_radix(str, &x, 16);
		assert(size == 8 && x == 0xFFFFFFFFu && x == 0xFFFFFFFF && (int32_t) x == -1);
	}
	{
		// TODO: figure out what to do in case trailling extra characters are present.
		const char *str = "7FFFFFFF Z";
		int32_t x;
		size_t size = scant_i32_radix(str, &x, 16);
		assert(size == 8 && x == 0x7FFFFFFF);
	}
	{
		const char *str = "2147483648";
		int32_t x;
		size_t size = scant_i32_radix(str, &x, 10);
		assert(size == 0);
	}
	{
		const char *str = "-001";
		int32_t x;
		size_t size = scant_i32_radix(str, &x, 10);
		assert(size == 4 && x == -1);
	}
	{
		const char *str = "-2147483648";
		int32_t x;
		size_t size = scant_i32_radix(str, &x, 10);
		assert(size == 11 && x == -2147483648);
	}
	{
		const char *str = "";
		int32_t x;
		size_t size = scant_i32_radix(str, &x, 10);
		assert(size == 0);
	}

	{
		const char *str = "+0";
		uint32_t x;
		size_t size = scant_u32_radix(str, &x, 10);
		assert(size == 2 && x == 0);
	}
	{
		const char *str = "-0";
		uint32_t x;
		size_t size = scant_u32_radix(str, &x, 10);
		assert(size == 0);
	}
	{
		const char *str = "0";
		uint32_t x;
		size_t size = scant_u32_radix(str, &x, 10);
		assert(size == 1 && x == 0);
	}
	{
		const char *str = "4294967295";
		uint32_t x;
		size_t size = scant_u32_radix(str, &x, 10);
		assert(size == 10 && x == 4294967295);
	}
	{
		const char *str = "-2147483648";
		int32_t x;
		size_t size = scant_i32_radix(str, &x, 10);
		assert(size == 11 && x == -2147483648);
	}
	{
		const char *str = "4294967296";
		uint32_t x;
		size_t size = scant_u32_radix(str, &x, 10);
		assert(size == 0);
	}
	{
		const char *str = " \n";
		uint32_t c;

		size_t size = scant_charset(str, &c, CHARSET_WHITESPACE_ASCII);
		assert(size == 1 && c == ' ');

		size = scant_charset(str + size, &c, CHARSET_WHITESPACE_ASCII);
		assert(size == 1 && c == '\n');
	}
	{
		const char *str = " \n";
		uint8_t byte;
		size_t size = scant_raw_u8(str, &byte);
		assert(size == 1 && byte == str[0]);
	}

	{
		const char *str = " \n";
		uint32_t c;

		size_t size = scant_char(str, &c, (uint32_t) str[0]);
		assert(size == 1 && c == ' ');
	}
	{
		const char *source = "stuv";
		uint32_t c;
		uint32_t delimiter[] = {'a', 'b', 't', 'x'};
		assert(sizeof(delimiter) / sizeof(uint32_t) == 4);

		size_t size = scant_delimiter(source, &c, delimiter, sizeof(delimiter) / sizeof(uint32_t));
		assert(size == 0);
	}
	{
		const char *source = "stuv";
		uint32_t c;
		uint32_t delimiter[] = {'a', 'b', 's', 'x'};
		assert(sizeof(delimiter) / sizeof(uint32_t) == 4);

		size_t size = scant_delimiter(source, &c, delimiter, sizeof(delimiter) / sizeof(uint32_t));
		assert(size == 1 && c == 's');
	}
	{
		const char *source = "stuv";
		uint32_t c;
		size_t size = scant_delimiter_str(source, &c, "abtx");
		assert(size == 0);
	}
	{
		const char *source = "stuv";
		uint32_t c;
		size_t size = scant_delimiter_str(source, &c, "absx");
		assert(size == 1 && c == 's');
	}
	{
		const char *source = "Abacus";
		uint32_t c;
		size_t size = scant_charset(source, &c, CHARSET_UPPERCASE_ASCII);
		assert(size == 1 && c == 'A');
	}
	{
		const char *source = "Abacus";
		uint32_t c;
		size_t size = scant_charset(source, &c, CHARSET_HEXDIGIT_ASCII);
		assert(size == 1 && c == 'A');
	}
	{
		const char *source = "Abacus";
		uint32_t c;
		size_t size = scant_charset(source, &c, CHARSET_DIGIT_ASCII);
		assert(size == 0);
	}
	{
		const char source[] = "Здравствуйте";
		uint32_t destination = 0;
		const size_t size = scant_char_any(source, &destination);
		assert(destination == 1047);
		assert(size == 2);
	}

	scanc_str_test();

	return 0;
}
