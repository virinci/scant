#ifndef SCANT_H_
#define SCANT_H_

#include <stddef.h>
#include <stdint.h>

// clang-format off

/// Read the first byte as an `u8` and return it.
/// This is true even for the case where the first byte is a `'\0'`.
/// Therefore, this function is guaranteed to return 1 for when `source` is not `NULL`.
size_t scant_raw_u8(const char *restrict source, uint8_t *restrict destination);

size_t scant_i32(const char *restrict str, int32_t *restrict destination);
size_t scant_i32_radix(const char *str, int32_t *restrict destination, uint32_t radix);

size_t scant_u32(const char *restrict str, uint32_t *restrict destination);
size_t scant_u32_radix(const char *restrict str, uint32_t *restrict destination, uint32_t radix);

// Char and string scanning
typedef enum {
	CHARSET_SPACE = 1 << 0,
	CHARSET_NEWLINE = 1 << 1,
	CHARSET_WHITESPACE_ASCII = 1 << 2,
	CHARSET_WHITESPACE = 1 << 3,

	CHARSET_DIGIT_ASCII = 1 << 8,
	CHARSET_DIGIT = 1 << 9,
	CHARSET_HEXDIGIT_ASCII = 1 << 10,
	CHARSET_HEXDIGIT = 1 << 11,

	CHARSET_UPPERCASE_ASCII = 1 << 16,
	CHARSET_UPPERCASE = 1 << 17,
	CHARSET_LOWERCASE_ASCII = 1 << 18,
	CHARSET_LOWERCASE = 1 << 19,

	// Combinators
	CHARSET_NONE = 0,
	CHARSET_ALPHABET_ASCII = CHARSET_UPPERCASE_ASCII | CHARSET_LOWERCASE_ASCII,

	// NOTE: This includes `CHARSET_HEXDIGIT_ASCII` as well due to including ASCII uppercase and lowercase.
	CHARSET_ALPHABET_DIGIT_ASCII = CHARSET_UPPERCASE_ASCII | CHARSET_LOWERCASE_ASCII | CHARSET_DIGIT_ASCII,
} ScantCharset;

size_t scant_char_any(const char *restrict str, uint32_t *restrict destination);

size_t scant_char(const char *restrict source, uint32_t *restrict destination, uint32_t c);
size_t scant_charset(const char *restrict source, uint32_t *restrict destination, ScantCharset charset);
size_t scant_delimiter(const char *restrict source, uint32_t *restrict destination, const uint32_t *restrict delimiter, size_t len);
size_t scant_delimiter_str(const char *restrict source, uint32_t *restrict destination, const char *restrict delimiter_str);

size_t scant_till_char(const char *restrict source, char *restrict destination, size_t len, int *restrict found, uint32_t c);
size_t scant_till_charset(const char *restrict source, char *restrict destination, size_t len, int *restrict found, ScantCharset charset);
size_t scant_till_delimiter(const char *restrict source, char *restrict destination, size_t len, int *restrict found, const uint32_t *restrict delimiter, size_t delimiter_len);
size_t scant_till_delimiter_str(const char *restrict source, char *restrict destination, size_t len, int *restrict found, const char *restrict delimiter_str);

// Scan while variants with a general form of (source, destination, destination_len, found, delimiter...).
size_t scant_while_char(const char *restrict source, char *restrict destination, size_t len, int *restrict found, uint32_t c);
size_t scant_while_charset(const char *restrict source, char *restrict destination, size_t len, int *restrict found, ScantCharset charset);
size_t scant_while_delimiter(const char *restrict source, char *restrict destination, size_t len, int *restrict found, const uint32_t *restrict delimiter, size_t delimiter_len);
size_t scant_while_delimiter_str(const char *restrict source, char *restrict destination, size_t len, int *restrict found, const char *restrict delimiter_str);

// clang-format on
#endif // SCANT_H_

// #define SCANT_IMPL
#if defined(SCANT_IMPL) && !defined(SCANT_IMPL_ALREADY)
#define SCANT_IMPL_ALREADY

#include <assert.h>
#include <float.h>
#include <math.h>
#include <stdbool.h>
#include <stdlib.h>

typedef enum {
	SCANT_NO_ERROR,

	SCANT_INVALID_LEADING_CHAR,
	SCANT_INVALID_CHAR,

	SCANT_INT_UNDERFLOW,
	SCANT_INT_OVERFLOW,
} ScantStatus;

size_t scant_raw_u8(const char *restrict source, uint8_t *restrict destination) {
	assert(source != NULL && destination != NULL);
	*destination = (uint8_t) *source;
	return 1;
}

static int8_t scant_char_to_digit(char ch, uint32_t radix) {
	assert(radix >= 2 && radix <= 36);

	const uint32_t c = (uint32_t) ch;
	int8_t digit = -1;

	if (radix <= 10) {
		if (c >= '0' && c < '0' + radix) {
			digit = (int8_t) (c - '0');
		}
	} else {
		if (c >= '0' && c <= '9') {
			digit = (int8_t) (c + '0');
		} else if (c >= 'a' && c < 'a' + radix - 10) {
			digit = (int8_t) (c + 10 - 'a');
		} else if (c >= 'A' && c < 'A' + radix - 10) {
			digit = (int8_t) (c + 10 - 'A');
		}
	}

	return digit;
}

size_t scant_i32_radix(const char *str, int32_t *destination, uint32_t radix) {
	assert(radix >= 2 && radix <= 36);
	assert(str != NULL && destination != NULL);

	int32_t num = 0;

	bool is_positive = true;
	size_t index = 0;

	if (str[index] == '-') {
		is_positive = false;
		index += 1;
	} else if (str[index] == '+') {
		index += 1;
	}

	int8_t digit = scant_char_to_digit(str[index], radix);
	if (digit < 0) {
		return 0;
	}

	while (digit >= 0) {
		const int32_t num_copy = num;

		num *= (int32_t) radix;
		num -= digit;

		// Detect overflow and underflow and abort in that case.
		if (is_positive && num == (1 << (32 - 1))) {
			// TODO: Integer overflow
			return 0;
		}

		if (num > num_copy) {
			// TODO: overflow if is_positive else underflow
			return 0;
		}

		index += 1;
		digit = scant_char_to_digit(str[index], radix);
	}

	*destination = is_positive ? -num : num;
	return index;
}

size_t scant_u32_radix(const char *restrict str, uint32_t *restrict destination, uint32_t radix) {
	assert(radix >= 2 && radix <= 36);
	assert(str != NULL && destination != NULL);

	uint32_t num = 0;
	size_t index = 0;

	if (str[index] == '+') {
		index += 1;
	}

	int8_t digit = scant_char_to_digit(str[index], radix);
	if (digit < 0) {
		return 0;
	}

	while (digit >= 0) {
		uint32_t num_copy = num;
		num *= (uint32_t) radix;
		num += (uint8_t) digit;

		if (num < num_copy) {
			// TODO: overflow
			return 0;
		}

		index += 1;
		digit = scant_char_to_digit(str[index], radix);
	}

	*destination = num;
	return index;
}

size_t scant_i32(const char *restrict str, int32_t *restrict destination) {
	return scant_i32_radix(str, destination, 10);
}

size_t scant_u32(const char *restrict str, uint32_t *restrict destination) {
	return scant_u32_radix(str, destination, 10);
}

size_t scant_f32(const char *restrict str, float *destination) {
	char *after = NULL;
	const float f = strtof(str, &after);
	const size_t count = (size_t) (after - str);

	if (f == 0.0f && count == 0) {
		// TODO: no conversion is performed.
		return count;
	} else if (f == HUGE_VALF || f == -HUGE_VALF) {
		return 0;
	} else if (fabsf(f) <= FLT_MIN) {
		return 0;
	}

	*destination = f;
	return count;
}

size_t scant_f64(const char *restrict str, double *destination) {
	char *after = NULL;
	double f = strtod(str, &after);
	const size_t count = (size_t) (after - str);

	if (f == 0.0 && count == 0) {
		// TODO: no conversion is performed.
		return count;
	} else if (f == HUGE_VAL || f == -HUGE_VAL) {
		return 0;
	} else if (fabs(f) <= DBL_MIN) {
		return 0;
	}

	*destination = f;
	return count;
}

size_t scant_bool(const char *restrict str, bool *destination) {
	assert(str != NULL && destination != NULL);

	if (str[0] == 't' && str[1] == 'r' && str[2] == 'u' && str[3] == 'e') {
		*destination = true;
		return 4;
	}

	if (str[0] == 'f' && str[1] == 'a' && str[2] == 'l' && str[3] == 's' && str[4] == 'e') {
		*destination = false;
		return 5;
	}

	return 0;
}

/// Reference: <https://github.com/skeeto/branchless-utf8/blob/master/utf8.h>
size_t scant_char_any(const char *restrict str, uint32_t *restrict destination) {
	assert(str != NULL && destination != NULL && str[0] != '\0');

	static const uint16_t indices[] = {0x0000, 0x0000, 0x0111, 0x0122, 0x0123};
	static const int masks[] = {0x00, 0x7f, 0x1f, 0x0f, 0x07};
	static const char lengths[] = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
								   0, 0, 0, 0, 0, 0, 0, 0, 2, 2, 2, 2, 3, 3, 4, 0};

	const uint32_t length = (uint32_t) lengths[(unsigned char) str[0] >> 3];
	const uint16_t idx = indices[length];

	uint32_t c = 0;
	c |= (uint32_t) (str[idx >> 12 & 0x0F] & masks[length]) << 18;
	c |= (uint32_t) (str[idx >> 8 & 0x0F] & 0x3F) << 12;
	c |= (uint32_t) (str[idx >> 4 & 0x0F] & 0x3F) << 6;
	c |= (uint32_t) (str[idx >> 0 & 0x0F] & 0x3F);
	c >>= 24 - 6 * length;

	*destination = c;
	return length;
}

size_t scant_char(const char *restrict source, uint32_t *restrict destination, uint32_t c) {
	uint32_t s = 0;
	const size_t count = scant_char_any(source, &s);
	if (count == 0) {
		// Error is bubbled up.
		return 0;
	}

	if (s != c) {
		// TODO: set error
		return 0;
	}

	*destination = s;
	return count;
}

size_t scant_delimiter(const char *restrict source, uint32_t *restrict destination,
					   const uint32_t *restrict delimiter, size_t len) {
	for (size_t i = 0; i < len; ++i) {
		const size_t count = scant_char(source, destination, delimiter[i]);
		if (count != 0) {
			return count;
		}
	}

	// TODO: set error that char in delimiter is not found
	return 0;
}

size_t scant_delimiter_str(const char *restrict source, uint32_t *restrict destination,
						   const char *restrict delimiter_str) {
	size_t idx = 0;

	while (delimiter_str[idx] != '\0') {
		uint32_t delimiter_char = 0;
		const size_t count = scant_char_any(delimiter_str + idx, &delimiter_char);
		assert(delimiter_char != '\0');
		if (count == 0) {
			break;
		}

		idx += count;

		uint32_t scanned_char = 0;
		const size_t count2 = scant_char(source, &scanned_char, delimiter_char);
		if (count2 != 0) {
			*destination = scanned_char;
			return count2;
		}
	}

	// TODO: set error that char in delimiter_str is not found
	return 0;
}

size_t scant_charset(const char *restrict source, uint32_t *restrict destination,
					 ScantCharset charset) {
	uint32_t c = 0;

	const size_t count = scant_char_any(source, &c);
	if (count == 0) {
		// Error is bubbled up.
		return 0;
	}

#define TEST_CHARSET(cs) ((charset & cs) == cs)

	if (TEST_CHARSET(CHARSET_SPACE)) {
		if (c == ' ') {
			*destination = c;
			return count;
		}
	}

	if (TEST_CHARSET(CHARSET_NEWLINE)) {
		if (c == '\n') {
			*destination = c;
			return count;
		}
	}

	if (TEST_CHARSET(CHARSET_WHITESPACE_ASCII)) {
		if (c == ' ' || c == '\t' || c == '\n' || c == '\v' || c == '\f' || c == '\r') {
			*destination = c;
			return count;
		}
	}

	if (TEST_CHARSET(CHARSET_WHITESPACE)) {
		assert(0 && "unimplemented");
	}

	if (TEST_CHARSET(CHARSET_DIGIT_ASCII)) {
		if (c >= '0' && c <= '9') {
			*destination = c;
			return count;
		}
	}

	if (TEST_CHARSET(CHARSET_DIGIT)) {
		assert(0 && "unimplemented");
	}

	if (TEST_CHARSET(CHARSET_HEXDIGIT_ASCII)) {
		if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'F') || (c >= 'a' && c <= 'f')) {
			*destination = c;
			return count;
		}
	}

	if (TEST_CHARSET(CHARSET_HEXDIGIT)) {
		assert(0 && "unimplemented");
	}

	if (TEST_CHARSET(CHARSET_UPPERCASE_ASCII)) {
		if (c >= 'A' && c <= 'Z') {
			*destination = c;
			return count;
		}
	}

	if (TEST_CHARSET(CHARSET_UPPERCASE)) {
		assert(0 && "unimplemented");
	}

	if (TEST_CHARSET(CHARSET_LOWERCASE_ASCII)) {
		if (c >= 'a' && c <= 'z') {
			*destination = c;
			return count;
		}
	}

	if (TEST_CHARSET(CHARSET_LOWERCASE)) {
		assert(0 && "unimplemented");
	}

	// NOTE: Combinators are already handled.

#undef TEST_CHARSET

	return 0;
}

#define SCANT_STRING(expr, test_expr)                                                              \
	assert(source != NULL && destination != NULL && well_ended != NULL);                           \
	*well_ended = 0;                                                                               \
                                                                                                   \
	for (; idx + 1 < len && source[idx] != '\0';) {                                                \
		count = expr;                                                                              \
		if (test_expr) {                                                                           \
			*well_ended = 1;                                                                       \
			break;                                                                                 \
		}                                                                                          \
                                                                                                   \
		if (count == 0) {                                                                          \
			count = 1;                                                                             \
		}                                                                                          \
                                                                                                   \
		const size_t next_idx = idx + count;                                                       \
		while (idx < next_idx) {                                                                   \
			destination[idx] = source[idx];                                                        \
			idx += 1;                                                                              \
		}                                                                                          \
	}                                                                                              \
                                                                                                   \
	destination[idx] = '\0';                                                                       \
	return idx;

size_t scant_till_char(const char *restrict source, char *restrict destination, size_t len,
					   int *restrict well_ended, uint32_t c) {
	size_t idx = 0;
	uint32_t s = 0;
	size_t count = 0;
	SCANT_STRING(scant_char(source + idx, &s, c), count != 0);
}

size_t scant_till_charset(const char *restrict source, char *restrict destination, size_t len,
						  int *restrict well_ended, ScantCharset charset) {
	size_t idx = 0;
	uint32_t s = 0;
	size_t count = 0;
	SCANT_STRING(scant_charset(source + idx, &s, charset), count != 0);
}

size_t scant_till_delimiter(const char *restrict source, char *restrict destination, size_t len,
							int *restrict well_ended, const uint32_t *restrict delimiter,
							size_t delimiter_len) {
	size_t idx = 0;
	uint32_t s = 0;
	size_t count = 0;
	SCANT_STRING(scant_delimiter(source + idx, &s, delimiter, delimiter_len), count != 0);
}

size_t scant_till_delimiter_str(const char *restrict source, char *restrict destination, size_t len,
								int *restrict well_ended, const char *restrict delimiter_str) {
	size_t idx = 0;
	uint32_t s = 0;
	size_t count = 0;
	SCANT_STRING(scant_delimiter_str(source + idx, &s, delimiter_str), count != 0);
}

size_t scant_while_char(const char *restrict source, char *restrict destination, size_t len,
						int *restrict well_ended, uint32_t c) {
	size_t idx = 0;
	uint32_t s = 0;
	size_t count = 0;
	SCANT_STRING(scant_char(source + idx, &s, c), count == 0);
}

size_t scant_while_charset(const char *restrict source, char *restrict destination, size_t len,
						   int *restrict well_ended, ScantCharset charset) {
	size_t idx = 0;
	uint32_t s = 0;
	size_t count = 0;
	SCANT_STRING(scant_charset(source + idx, &s, charset), count == 0);
}

size_t scant_while_delimiter(const char *restrict source, char *restrict destination, size_t len,
							 int *restrict well_ended, const uint32_t *restrict delimiter,
							 size_t delimiter_len) {
	size_t idx = 0;
	uint32_t s = 0;
	size_t count = 0;
	SCANT_STRING(scant_delimiter(source + idx, &s, delimiter, delimiter_len), count == 0);
}

size_t scant_while_delimiter_str(const char *restrict source, char *restrict destination,
								 size_t len, int *restrict well_ended,
								 const char *restrict delimiter_str) {
	size_t idx = 0;
	uint32_t s = 0;
	size_t count = 0;
	SCANT_STRING(scant_delimiter_str(source + idx, &s, delimiter_str), count == 0);
}

#undef SCANT_STRING

#endif // defined(SCANT_IMPL) && !defined(SCANT_ALREADY_IMPL)
