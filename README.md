## scant
A simple header-only C library for type-safe scanning of primitive data types from a string. This library provides an alternative to the `scanf` family of functions.

## Instructions
The user is expected to write their own functions for scanning complex things. They can use the primitive scanning functions provided by the library to accomplish that.
See `scant_test.c` for examples on how to use it.

## Types
- [x] raw u8 (byte)
- [ ] i8
- [ ] u8
- [ ] i16
- [ ] u16
- [x] i32
- [x] u32
- [ ] i64
- [ ] u64
- [ ] i128
- [ ] u128
- [ ] f32
- [ ] f64
- [x] UTF-8 char
- [ ] str
- [ ] word?
- [ ] line
- [ ] whitespaces

### Interesting Character Sets
- Whitespace (+ ASCII)
- Digit (+ ASCII)
- Uppercase (+ ASCII)
- Lowercase (+ ASCII)
- ASCII control

## Terminology
- char: An `u32` representing a Unicode codepoint.
- charset: An integer bitmask representing a combination of commonly used character sets.
- delimiter: A sized array of `u32` where each element is a Unicode codepoint.
- delimiter_str: A null-terminated string that is a sequence of UTF-8 encoded Unicode codepoints. These must be first parsed into `u32` codepoints.
- string: A UTF-8 encoded string that is represented by a `char *`.
- raw_u8: when `0` is parsed into an `u8` as `48` (UTF-8 codepoint for `'0'`) instead of `0`.

## To-Do
- [ ] Don't read integers with overflow/underflow. If overflow/underflow is the desired behavior then user can cast wider integers to the desidered integer size.
- [ ] Implement integer types other than 32 bit. Write a code generator for other int sizes.
- [ ] Return 0 for the number of bytes read on error but also use a thread-local struct for reporting the nature of error in case the user might be interested in knowing.
- [ ] Make the feature for scanning from a file optional. User should perform the IO instructions on the file and then scan from the returned string.
- [x] Throw an error when any irrelevant leading characters are present in the string (even whitespaces).
- [ ] What about irrelevant trailing characters? `7FFFFFFFF` the last `F` is relevant and it causes an overflow for i32. `7FFFFFFFZ` the last `Z` however is irrelevant as it doesn't come under base 16.
- [ ] Make this library header only and no `.h` file extension.

## References

### Integer
- <https://codereview.stackexchange.com/questions/37177/simple-method-to-detect-int-overflow>
- <https://www.codespeedy.com/detect-integer-overflow-in-cpp/>
- <https://stackoverflow.com/questions/199333/how-do-i-detect-unsigned-integer-overflow>
- <https://www.fefe.de/intof.html>

### Float
- <https://stackoverflow.com/questions/85223/how-to-manually-parse-a-floating-point-number-from-a-string>
- <https://docs.sun.com/source/806-3568/ncg_goldberg.html>

### Branchless UTF-8
- <https://nullprogram.com/blog/2017/10/06/>
- <https://github.com/skeeto/branchless-utf8>
- <https://github.com/dotnet/corefxlab/issues/1831>
- <https://www.reddit.com/r/cpp/comments/74xii5/a_branchless_utf8_decoder/>
- <https://news.ycombinator.com/item?id=15423674>
- <https://blogs.perl.org/users/nick_wellnhofer/2015/04/branchless-utf-8-length.html>
- <https://internals.rust-lang.org/t/interest-in-branchless-utf-8-decoder/6014>
- <https://bjoern.hoehrmann.de/utf-8/decoder/dfa/>

### File Handling
- <https://stackoverflow.com/questions/4917801/using-fseek-with-a-file-pointer-that-points-to-stdin>
- <https://stackoverflow.com/questions/16672672/can-fseekstdin-1-seek-set-or-rewindstdin-be-used-to-flush-the-input-buffer-i>
